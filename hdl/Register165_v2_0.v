
`timescale 1 ns / 1 ps

	module Register165_v2_0 #
	(
		// Users to add parameters here
        parameter integer C_CLOCK_PRESCALLER = 3,
        parameter integer C_REGISTER165_COUNT = 3,
        parameter integer C_DEBOUNCE_TAPS = 6,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 5
	)
	(
		// Users to add ports here
        input wire SERIAL_IN,
		output wire SERIAL_CLK,
		output wire SERIAL_CLKEN,
		output wire SERIAL_LOAD,
		output wire irq,
        output wire need_latch,
        output wire data_in_sync,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);

    wire [C_REGISTER165_COUNT*8-1:0] DATA_OUT_ISR;
    wire [C_REGISTER165_COUNT*8-1:0] DATA_OUT_DEBOUNCED;
    wire [C_REGISTER165_COUNT*8-1:0] DATA_OUT_ENABLED;
    wire [C_REGISTER165_COUNT*8-1:0] DATA_OUT;
    wire DATA_READY;
    wire [C_REGISTER165_COUNT*8-1:0] ENABLE;
    wire [C_REGISTER165_COUNT*8-1:0] DEBOUNCE;
    wire [C_REGISTER165_COUNT*8-1:0] INVERT;

    wire INTERRUPT_ENABLE;
    wire INTERRUPT_ACK;
    wire GIER;
    wire INTERRUPT_STATUS;

// Instantiation of Axi Bus Interface S00_AXI
	Register165_v2_0_S00_AXI # (
		.C_DATAREG_WIDTH   (C_REGISTER165_COUNT*8),
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) Register165_v2_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),

		.DATAREG      (DATA_OUT_ISR),
		.ENABLE       (ENABLE),
		.DEBOUNCE     (DEBOUNCE),
		.INVERT       (INVERT),

        .INTERRUPT_ENABLE(INTERRUPT_ENABLE),
        .INTERRUPT_ACK   (INTERRUPT_ACK),
        .GIER            (GIER),
        .INTERRUPT_STATUS(INTERRUPT_STATUS)
	);

	// Add user logic here
	wire aclk = s00_axi_aclk;
	wire aresetn = s00_axi_aresetn;
	

    SerialToParallel # (
    	.C_CLOCK_PRESCALLER(C_CLOCK_PRESCALLER),
    	.C_REGISTER_COUNT  (C_REGISTER165_COUNT)
    ) SerialToParallel_u (
    	.aclk      (aclk),
    	.aresetn   (aresetn),
    	.REG_CLK   (SERIAL_CLK),
    	.REG_CLKEN (SERIAL_CLKEN),
    	.REG_LOAD  (SERIAL_LOAD),
    	.REG_DATAIN(SERIAL_IN),
    	.DATA_OUT  (DATA_OUT),
    	.DATA_READY(DATA_READY),
        .need_latch(need_latch),
        .data_in_sync(data_in_sync)
    );
    

    EnableLogic #(
		.C_DATA_WIDTH(C_REGISTER165_COUNT*8)
	) EnableLogic_u (
		.aclk(aclk),
		.aresetn(aresetn),
		.data_in(DATA_OUT),
		.enable  (ENABLE),
		.data_out(DATA_OUT_ENABLED)
	);
    
wire DATA_READY_DELAYED;

	DelayLine #(
		.TAPS(1)
	) dl_dataready (
		.aclk      (aclk),
		.aresetn   (aresetn),
		.signal_in (DATA_READY),
		.signal_out(DATA_READY_DELAYED)
	);

    Debounce #(
    	.C_DATA_WIDTH   (C_REGISTER165_COUNT*8),
    	.C_DEBOUNCE_TAPS(C_DEBOUNCE_TAPS)
	) debounce_u (
		.aclk           (aclk),
		.aresetn        (aresetn),
		.DATA_IN        (DATA_OUT_ENABLED),
		.DATA_READY     (DATA_READY_DELAYED),
		.DEBOUNCE_ENABLE(DEBOUNCE),
		.DATA_OUT       (DATA_OUT_DEBOUNCED)
	);

    Interrupt #(
        .C_DATAREG_WIDTH(C_REGISTER165_COUNT*8)
    ) interrupt_u (
        .aclk         (aclk),
        .aresetn      (aresetn),
        .DATA_IN      (DATA_OUT_DEBOUNCED),
        .DATA_OUT     (DATA_OUT_ISR),
        .GIER         (GIER),
        .interrupt_enable(INTERRUPT_ENABLE),
        .interrupt_ack   (INTERRUPT_ACK),
        .interrupt_status(INTERRUPT_STATUS),
        .interrupt       (irq)
    );
        
	// User logic ends

endmodule

module EnableLogic #(
		parameter integer C_DATA_WIDTH=24
	)
	(
		input wire aclk,
		input wire aresetn,
		input wire[C_DATA_WIDTH-1:0] data_in,
		input wire[C_DATA_WIDTH-1:0] enable,
		output wire[C_DATA_WIDTH-1:0] data_out
	);

bit [C_DATA_WIDTH-1:0] data_dff;
	always_ff @(posedge aclk)
		if (aresetn == '0)
			data_dff <= '0;
		else
			data_dff <= data_in & enable;

	assign data_out = data_dff;
endmodule


module DelayLine #(
		parameter integer TAPS=2
	)
	(
		input wire aclk,
		input wire aresetn,
		input wire signal_in,
		output wire signal_out
	);

bit [TAPS-1:0] taps;
	generate
		if (TAPS == 1) begin
			always_ff @(posedge aclk)
				if (aresetn == '0)
					taps[0] <= '0;
				else
					taps[0] <= signal_in;
			assign signal_out = taps[0];
		end
		else begin
			always_ff @(posedge aclk)
				if (aresetn == '0)
					taps <= '0;
				else
					taps <= {taps[TAPS-2:0], signal_in};
			assign signal_out = taps[TAPS-1];			
		end
	endgenerate	
endmodule

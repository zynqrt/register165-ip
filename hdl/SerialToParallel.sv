module SerialToParallel #
	(
		parameter integer C_CLOCK_PRESCALLER = 3,
		parameter integer C_REGISTER_COUNT = 3
	)
	(
		input wire  aclk,
		input wire  aresetn,

		output wire REG_CLK,
		output wire REG_CLKEN,
		output wire REG_LOAD,
		input wire REG_DATAIN,

		output wire [C_REGISTER_COUNT * 8 - 1 : 0] DATA_OUT,
		output wire DATA_READY,
		output wire need_latch,
		output wire data_in_sync
	);

	localparam C_CLOCK_PRESCALLER_WIDTH = $clog2(C_CLOCK_PRESCALLER + 1);
	localparam C_BIT_COUNTER_WIDTH = $clog2(C_REGISTER_COUNT * 8 + 1);

bit clk_reg;
bit [C_CLOCK_PRESCALLER_WIDTH - 1 : 0] clk_prescaller;

	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			clk_reg <= 0;
			clk_prescaller <= 0;
		end
		else begin
			if (clk_prescaller < C_CLOCK_PRESCALLER) begin
				clk_prescaller <= clk_prescaller + 1;
			end
			else begin
				clk_prescaller <= 0;
				clk_reg <= ~clk_reg;
			end
		end
	end

assign REG_CLK = clk_reg;
wire clock_rise = (clk_prescaller == C_CLOCK_PRESCALLER) & (~clk_reg);
wire clock_fall = (clk_prescaller == C_CLOCK_PRESCALLER) & clk_reg;


bit data_in_dff1;
bit data_in_dff2;
	always_ff @(posedge aclk) begin
		data_in_dff1 <= REG_DATAIN;
		data_in_dff2 <= data_in_dff1;
	end

bit [C_REGISTER_COUNT*8 - 1 : 0] data_out;
bit [C_REGISTER_COUNT*8 - 1 : 0] shift_reg;

bit clken_reg;
bit load_reg;
bit [C_BIT_COUNTER_WIDTH - 1 : 0] bit_cnt;
bit last;

	enum bit [1:0] {
		IDLE = 'h0,
		LOAD,
		WAIT_CLKEN,
		SHIFT
	} state;

	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			state <= IDLE;
			clken_reg <= 1;
			load_reg <= 1;
			bit_cnt <= 0;

			data_out <= 0;
			shift_reg <= 0;
			last <= 0;
		end
		else begin
			last <= 0;
			case (state)
				IDLE: begin
					clken_reg <= 1;
					bit_cnt <= 0;
					if (clock_rise) begin
						load_reg <= 0;
						state <= LOAD;
					end
				end

				LOAD: begin
					if (clock_rise) begin
						load_reg <= 1;
						state <= WAIT_CLKEN;
					end
				end
				
				WAIT_CLKEN: begin
					if (clock_fall) begin
						clken_reg <= 0;
						state <= SHIFT;
					end
				end

				SHIFT: begin
					if (clock_fall) begin
						bit_cnt <= bit_cnt + 1;
						if (bit_cnt == C_REGISTER_COUNT * 8 - 1) begin
							data_out <= shift_reg;
							state <= IDLE;
							clken_reg <= 1;
							last <= 1;
						end
					end
				end
			endcase

			if (need_latch) begin
				shift_reg <= {shift_reg[C_REGISTER_COUNT * 8 - 2 : 0], data_in_dff2};
			end
		end
	end

assign need_latch = ((state == SHIFT) || (state == WAIT_CLKEN)) & clock_rise;
assign data_in_sync = data_in_dff2;

assign DATA_READY = last;
assign REG_LOAD = load_reg;
assign REG_CLKEN = clken_reg;
assign DATA_OUT = data_out;
	
endmodule

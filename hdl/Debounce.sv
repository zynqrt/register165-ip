module Debounce #
	(
		parameter integer C_DATA_WIDTH = 24,
		parameter integer C_DEBOUNCE_TAPS = 3
	)
	(
	   input wire aclk,
	   input wire aresetn,
	   input wire [C_DATA_WIDTH-1:0] DATA_IN,
	   input wire [C_DATA_WIDTH-1:0] DEBOUNCE_ENABLE,
	   input wire DATA_READY,
	   output wire [C_DATA_WIDTH-1:0] DATA_OUT
	);
	
genvar i;
    generate
    	for (i = 0; i < C_DATA_WIDTH; i = i + 1) begin
    		DebounceChannel #(
    			.C_DEBOUNCE_TAPS(C_DEBOUNCE_TAPS)
    		) deb_ch (
    			.aclk      (aclk),
    			.aresetn   (aresetn),
    			.enable    (DEBOUNCE_ENABLE[i]),
    			.data_in   (DATA_IN[i]),
    			.data_ready(DATA_READY),
    			.data_out  (DATA_OUT[i])
    		);
    	end
    endgenerate
endmodule


module DebounceChannel #
	(
		parameter integer C_DEBOUNCE_TAPS = 3
	)
	(
		input wire aclk,
		input wire aresetn,
		input wire enable,
		input wire data_in,
		input wire data_ready,
		output wire data_out
	);

bit [C_DEBOUNCE_TAPS-1:0] taps_dff;
	
	always_ff @(posedge aclk) begin
		if (aresetn == '0) begin
			taps_dff <= '0;
		end
		else begin
			if (data_ready)
				taps_dff <= {taps_dff[C_DEBOUNCE_TAPS-2:0], data_in};
		end
	end

wire all_ones = &taps_dff;
wire all_zeros = ~|taps_dff;

bit outstate;
	always_ff @(posedge aclk)
		if (aresetn == '0)
			outstate <= '0;
		else
			if (all_ones)
				outstate <= '1;
			else if (all_zeros)
				outstate <= '0;
			else
				outstate <= outstate;

bit out_dff;
	always_ff @(posedge aclk)
		if (aresetn == '0)
			out_dff <= '0;
		else
			out_dff <= (enable == '1) ? outstate : data_in;
	assign data_out = out_dff;
endmodule

module Interrupt #(
        parameter integer C_DATAREG_WIDTH=24
    )
    (
        input wire aclk,
        input wire aresetn,
        input wire[C_DATAREG_WIDTH-1:0] DATA_IN,
        output wire [C_DATAREG_WIDTH-1:0] DATA_OUT,

        input wire interrupt_enable,
        input wire GIER, // Global interrupt enable
        input wire interrupt_ack, // 1 clear flag
        output bit interrupt_status,
        output wire interrupt 
    );


bit[C_DATAREG_WIDTH-1:0] datain_dff;   
    always_ff @(posedge aclk)
        if (aresetn == '0)
            datain_dff <= '0;
        else
            datain_dff <= DATA_IN;
    assign DATA_OUT = datain_dff;

bit [C_DATAREG_WIDTH-1:0] datareg_xor;
    always_ff @(posedge aclk)
        if (aresetn == '0)
            datareg_xor <= '0;
        else
            datareg_xor <= datain_dff^DATA_IN;

bit data_changed;
    always_ff @(posedge aclk) 
        if (aresetn == '0)
            data_changed <= '0;
        else
            data_changed <= |datareg_xor;

    always_ff @(posedge aclk)
        if (aresetn == '0)
            interrupt_status <= '0;
        else begin
            if (data_changed) begin
                interrupt_status <= '1;
            end
            if (interrupt_ack) begin
                interrupt_status <= '0;
            end
        end

    assign interrupt = interrupt_status & interrupt_enable & GIER;
endmodule

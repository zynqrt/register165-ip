
#ifndef REGISTER165_H
#define REGISTER165_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"

#define REGISTER165_S00_AXI_SLV_REG_DATA_OFFSET 0
#define REGISTER165_S00_AXI_SLV_REG_ENABLE_OFFSET 4
#define REGISTER165_S00_AXI_SLV_REG_DEBOUNCE_OFFSET 8
#define REGISTER165_S00_AXI_SLV_REG_INVERT_OFFSET 12
#define REGISTER165_S00_AXI_SLV_REG_IPIER_OFFSET 16
#define REGISTER165_S00_AXI_SLV_REG_IPISR_OFFSET 20
#define REGISTER165_S00_AXI_SLV_REG_GIER_OFFSET 24


/**************************** Type Definitions *****************************/
/**
 *
 * Write a value to a REGISTER165 register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the REGISTER165device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void REGISTER165_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define REGISTER165_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a REGISTER165 register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the REGISTER165 device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 REGISTER165_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define REGISTER165_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the REGISTER165 instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus REGISTER165_Reg_SelfTest(void * baseaddr_p);

#endif // REGISTER165_H

# Register165 IP

# Description

# Port map

# Register map

# Utilization

# Испытания

Конфигурация
 1. AXI Clock: 80 MHz, 
 2. Clock prescaller: 2, 
 3. Debounce: 10.
 
Реакция на прерывание Baremetal:
 1. Без Debounce: 880 нс
 2. С Debounce: 880 нс

Реакция на прерывание Linux Xenomai:

 1. Без Debounce: 6.8 - 7.5 мкс. С полной загрузкой сети время реакции не изменилось
 2. С Debounce: теже значения, что и без.
 
